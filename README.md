The MSeg Motion Segmentation Evaluation Framework
===

**MSeg** is a framework for the [ArmarX](https://armarx.humanoids.kit.edu/index.html) robot development environment,
built to run and evaluate motion segmentation algorithms written in C++, Java, Python or MATLAB. Please refer to the
[MSeg documentation](https://mseg.readthedocs.io/) for guides on how to install the framework and how to get started.

If you encounter a problem, feel free to open a new issue in the
[issue tracker](https://gitlab.com/h2t/kit-mseg/mseg/issues) or send an email to
<a href="mailto:incoming+h2t/kit-mseg/mseg@gitlab.com">incoming+h2t/kit-mseg/mseg@gitlab.com</a>
(this will create an issue in the issue tracker).

--- *The rest of this document is relevant for developers only* ---


# MSeg Umbrella Repository

This is the umbrella repository for MSeg which bundles tools to setup, handle and package all other repositories. 


## Development

To get started, clone this repository. For packaging, you'd want to checkout a particular release now as well.
Releases of this repository are coupled with the MSeg core module.

All of the following make rules are designed to be idempotent. If an error occurs, you may run the same command again.
Sometimes this resolves the issue.

- Run `make init`, which will download all repositories, checkout the correct release and check the system dependencies
  which include deb, pip and pip3. You may get errors about missing dependencies etc. In such case, running the
  suggested commands should resolve the problems. Afterwards, run `make init` again to ensure that everything is setup 
  properly.
- Run `make install` to setup, compile and/or build the projects. For each project, the corresponding install rule will
  be executed. The make rule will take care of the proper order. To speed up the installation process, you may use extra
  threads. This can be done by setting the `THREADS` environment variable. Example using four threads:
  `THREADS=4 make install`.
- Run `make package` to build `.deb` packages. For each project, the corresponding package rule will be executed.
  Afterwards, the `.deb` artefacts will be collected and copied to `./deb-dist`. Please note that this rule may fail if
  you did not checkout a particular release beforehand.


# License

Licensed under the [GPL 2.0 License](./LICENSE.md).
