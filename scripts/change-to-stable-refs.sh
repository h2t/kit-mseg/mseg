#!/bin/bash

set -e

# Source helper functions
source ./scripts/helperfns.sh

# Get config variables
source ./stable-refs.cfg

print_separator "Changing to stable"

# If $core_ref is set, checkout that ref
if [ ! -z ${core_ref+x} ]; then
    (cd core && git checkout ${core_ref})
else
    echo "\$core_ref not set in config. Leaving \`core\` on master"
fi

# If $core_ice_ref is set, checkout that ref
if [ ! -z ${core_ice_ref+x} ]; then
    (cd core-ice && git checkout ${core_ice_ref})
else
    echo "\$core_ice_ref not set in config. Leaving \`core-ice\` on master"
fi

# If $pli_cpp_ref is set, checkout that ref
if [ ! -z ${pli_cpp_ref+x} ]; then
    (cd pli-cpp && git checkout ${pli_cpp_ref})
else
    echo "\$pli_cpp_ref not set in config. Leaving \`pli-cpp\` on master"
fi

# If $pli_java_ref is set, checkout that ref
if [ ! -z ${pli_java_ref+x} ]; then
    (cd pli-java && git checkout ${pli_java_ref})
else
    echo "\$pli_java_ref not set in config. Leaving \`pli-java\` on master"
fi

# If $pli_python_ref is set, checkout that ref
if [ ! -z ${pli_python_ref+x} ]; then
    (cd pli-python && git checkout ${pli_python_ref})
else
    echo "\$pli_python_ref not set in config. Leaving \`pli-python\` on master"
fi

# If $pli_matlab_ref is set, checkout that ref
if [ ! -z ${pli_matlab_ref+x} ]; then
    (cd pli-matlab && git checkout ${pli_matlab_ref})
else
    echo "\$pli_matlab_ref not set in config. Leaving \`pli-matlab\` on master"
fi

# If $mseg_tools_ref is set, checkout that ref
if [ ! -z ${mseg_tools_ref+x} ]; then
    (cd mseg-tools && git checkout ${mseg_tools_ref})
else
    echo "\$mseg_tools_ref not set in config. Leaving \`mseg-tools\` on master"
fi

# If $documentation_ref is set, checkout that ref
if [ ! -z ${documentation_ref+x} ]; then
    (cd documentation && git checkout ${documentation_ref})
else
    echo "\$documentation_ref not set in config. Leaving \`documentation\` on master"
fi
