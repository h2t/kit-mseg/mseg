#!/bin/bash

set -e

# Source helper functions
source ./scripts/helperfns.sh

print_separator "Building bashrc file"

# General local vars
mseg_home_dir=".mseg"
mseg_bashrc="${HOME}/${mseg_home_dir}/mseg-bashrc.sh"
mseg_bashrc_source="source \${HOME}/${mseg_home_dir}/mseg-bashrc.sh"

# Add source command for the MSeg bashrc file if it isn't added yet
if ! grep "^${mseg_bashrc_source}" ${HOME}/.bashrc > /dev/null; then
    echo "Adding command to source the mseg-bashrc to \`~/.bashrc\`"
    printf "\n# Source MSeg bashrc file\n" >> ${HOME}/.bashrc
    printf "${mseg_bashrc_source}\n" >> ${HOME}/.bashrc
else
    echo "The mseg-bashrc is already getting sourced in \`~/.bashrc\`"
fi

echo "Fetching mseg-bashrc entries"
bashrc_entries=$(make --no-print-directory bashrc)

# Calculate the MD5 hash of those entries
current_hash=$(echo "${bashrc_entries}" | md5sum | awk '{print $1}' 2> /dev/null)

# Build MSeg bashrc file
echo "Writing mseg-bashrc to \`${mseg_bashrc}\`"
printf "#!/bin/bash\n\n" > ${mseg_bashrc}
printf "# This file is auto-generated.\n# DO NOT MAKE ANY CHANGES.\n\n" >> ${mseg_bashrc}
printf "# MD5 hash of the following bashrc entries to identify unsourced changes\n" >> ${mseg_bashrc}
printf "export MSEG_BASHRC_INDICATOR=\"${current_hash}\"\n\n" >> ${mseg_bashrc}
printf "${bashrc_entries}" >> ${mseg_bashrc}

echo "Current hash:  ${current_hash}"
echo "Expected hash: ${MSEG_BASHRC_INDICATOR}"

# Check if the current hash matches the one deposited in $MSEG_BASHRC_INDICATOR.
# If they are not equal a resourcing needs to be performed
if [ ! "${MSEG_BASHRC_INDICATOR}" = "${current_hash}" ]; then
    echo "ERROR: bashrc-file changed and needs to be resourced. Run \`source ~/.bashrc\`" && false
else
    echo "Current bashrc is up-to-date"
fi
