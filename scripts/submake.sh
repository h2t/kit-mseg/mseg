#!/bin/bash

set -e

# Source helper functions
source ./scripts/helperfns.sh

ACTION_HR=""
ACTION=$1

SILENT=false

if [ "${ACTION}" = "install" ]; then
    ACTION_HR="Building"
elif [ "${ACTION}" = "package" ]; then
    ACTION_HR="Packaging"
else
    SILENT=true
fi

if [ ${ACTION:0:1} = "." ]; then
    make_failsafe "." ${ACTION}
fi

print_separator "${ACTION_HR} the MSeg Ice interfaces" ${SILENT}
make_failsafe "core-ice" ${ACTION}

print_separator "${ACTION_HR} the MSeg core module" ${SILENT}
make_failsafe "core" ${ACTION}

print_separator "${ACTION_HR} the C++ PLI" ${SILENT}
make_failsafe "pli-cpp" ${ACTION}

print_separator "${ACTION_HR} the Java PLI" ${SILENT}
make_failsafe "pli-java" ${ACTION}

print_separator "${ACTION_HR} the Python PLI" ${SILENT}
make_failsafe "pli-python" ${ACTION}

print_separator "${ACTION_HR} the MATLAB PLI" ${SILENT}
make_failsafe "pli-matlab" ${ACTION}

print_separator "${ACTION_HR} mseg* tools" ${SILENT}
make_failsafe "mseg-tools" ${ACTION}

print_separator "${ACTION_HR} documentation" true
make_failsafe "documentation" ${ACTION}

print_separator "${ACTION_HR}: FINISHED SUCCESSFULLY" ${SILENT}
