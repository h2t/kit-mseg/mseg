#!/bin/bash

set -e

# Source helper functions
source ./scripts/helperfns.sh

print_separator "Cloning repositories"

# Clone repos

(git_clone_mseg_repo "core" 2>&1 | grep -v "fatal: destination path") \
    || echo "Repo 'core' already cloned"

git_clone_mseg_repo "core-ice" 2>&1 | grep -v "fatal: destination path" \
    || echo "Repo 'core-git' already cloned"

git_clone_mseg_repo "pli-cpp" 2>&1 | grep -v "fatal: destination path" \
    || echo "Repo 'pli-cpp' already cloned"

git_clone_mseg_repo "pli-java" 2>&1 | grep -v "fatal: destination path" \
    || echo "Repo 'pli-java' already cloned"

git_clone_mseg_repo "pli-python" 2>&1 | grep -v "fatal: destination path" \
    || echo "Repo 'pli-python' already cloned"

git_clone_mseg_repo "pli-matlab" 2>&1 | grep -v "fatal: destination path" \
    || echo "Repo 'pli-matlab' already cloned"

git_clone_mseg_repo "mseg-tools" 2>&1 | grep -v "fatal: destination path" \
    || echo "Repo 'mseg-tools' already cloned"

git_clone_mseg_repo "datasets" 2>&1 | grep -v "fatal: destination path" \
    || echo "Repo 'dataset' already cloned"

git_clone_mseg_repo "documentation" 2>&1 | grep -v "fatal: destination path" \
    || echo "Repo 'documentation' already cloned"
